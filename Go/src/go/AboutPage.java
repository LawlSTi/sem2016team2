/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package go;

//import javafx.scene.Scene;
//import javafx.stage.Stage;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 *
 * @author Khang
 *
 * 
 */
public class AboutPage 
{

       public AboutPage(){        
        
       Stage window = new Stage(); //originally in Go.Java, creates the window
       GridPane grid = new GridPane();
       grid.setAlignment(Pos.TOP_LEFT);
       grid.setHgap(10);                      
       grid.setVgap(10);                      
       grid.setPadding(new Insets(25, 25, 25, 25));
               
       Scene about = new Scene(grid, 718, 404);
       window.setScene(about);
       window.setResizable(false);
       window.sizeToScene();
       grid.getStylesheets().add("GoCSS/GoAbout.css");
       window.show();
       window.setTitle("Knowing the Go Team!");
                
                
                //places text onto the window
       Text sceneown = new Text("Product Owner: Dr.Brian Eddy \nCompany: University of West Florida \nDepartment: Software Engineering Management \n");
       sceneown.setId("about-text");
       sceneown.setFont(new Font(20));
       grid.add(sceneown, 0, 0, 3, 1);
      
       Text sceneteam = new Text("Team Developers: \n\n");
       sceneteam.setId("about-text");
       sceneteam.setFont(new Font(20));
       grid.add(sceneteam, 0, 1, 9, 10);
       
       Text name1 = new Text("Lauren Jones:  Graphics Designer, Software Developer \n");
       name1.setId("about-text");
       name1.setFont(new Font(17));
       grid.add(name1, 0, 1, 10 , 14);
       
       Text name2 = new Text("Corey Loftis: Software Developer \n");
       name2.setId("about-text");
       name2.setFont(new Font(17));
       grid.add(name2, 0, 3, 12, 16);

       Text name3 = new Text("Chris Robinson: Graphics Designer, Software Developer \n");
       name3.setId("about-text");
       name3.setFont(new Font(17));
       grid.add(name3, 0, 5, 14, 18);

       Text name4 = new Text("Paul Gartner: Software Developer \n");
       name4.setId("about-text");
       name4.setFont(new Font(17));
       grid.add(name4, 0, 7, 16, 20);

       Text name5 = new Text("Khang Hoang: Software Developer \n");
       name5.setId("about-text");
       name5.setFont(new Font(17));
       grid.add(name5, 0, 9, 18, 22);
            
    }
    
}
