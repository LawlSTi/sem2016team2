/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package go;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Corey + Michael
 */
public class GoBoardController implements Initializable {
    @FXML
    private ImageView goBoardImageView;
    @FXML
    private Button boardBackButton;
    @FXML
    private AnchorPane boardPane;
    
    private GoBoard goBoard;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        //Creating a new GoBoard object on initilization of the controller
        goBoard = new GoBoard(boardPane);
    }    

    @FXML
    private void BackToMenuFromBoard(ActionEvent event) throws IOException {
        //mainStage references the Go board's window
        Stage mainStage = (Stage)boardBackButton.getScene().getWindow();
        mainStage.setScene(CreateMainScene());
    }

    @FXML
    private void XYDetection(MouseEvent event) {
        goBoard.setStoneWhite(event.getSceneX(), event.getSceneY());
        //System.out.println("X Cor = "+ event.getSceneX() + "\n Y Cor = "+ event.getSceneY()+"\n"); //Test code
    }    
    
    //This method grabs the MainMenu's scene to be used in 
    // switching back to the MainMenu from the Go board window.
    private Scene CreateMainScene() throws IOException{
        URL url = Go.class.getResource("/GoFXML/mainmenu.fxml");
        Parent root = FXMLLoader.load(url); 
        
        Scene mainScene = new Scene(root);
        mainScene.getStylesheets().add("GoCSS/mainmenustyle.css");
        
        return mainScene;
    }
    
}