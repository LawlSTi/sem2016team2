/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package go;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author lawls
 */
public class Tutorials {
    
       public Tutorials () throws Exception {
        
       Stage window = new Stage();
       Parent root = FXMLLoader.load(getClass().getResource("/GoFXML/tutorial.fxml"));
       Scene scene = new Scene(root);
       scene.getStylesheets().add("/GoCSS/tutorialSelectStylesheet.css");
       window.setScene(scene);
       window.setResizable(false);
       window.sizeToScene();
       window.show();
       window.setTitle("Tutorials");
    }
}
