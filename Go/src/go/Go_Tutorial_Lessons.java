//author: Paul Gartner
package go;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.input.InputEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import java.util.EventObject;
import java.util.EventListener;
import java.awt.event.ActionListener;
import javafx.geometry.Pos;

public class Go_Tutorial_Lessons 
{
    //Lessons 1-11. Text has been formatted to fit Lesson_Text_Box() window.
    public void L1_Stone_Placement()
    {
        String title = "1. Stone Placement";
        String content = "There are two players in Go; the black player (who always places their stone first when a game begins) "
                + "\n and the white player (who always receives a handicap score of at least 0.5 since black always has an advantage). "
                + "\n\nFor the purposes of this tutorial you will be playing as white. "
                + "\n\nTry placing a stone on the board.";        
        
        Lesson_Text_Box(title, content);        
    }
    
    public void L2_Single_Capture()
    {
        String title = "2. Single Capture";
        String content = " To capture an enemy stone you must surround it on all sides, vertically and horizontally, diagonal does not count. "
                + "\n\n Try capturing a black stone. ";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L3_No_Suicide()
    {
        String title = "3. No Suicidal Moves";
        String content = " You may not place a stone in a position in which it will be immediately captured by your opponent. "
                + "\n\n Try to place a stone in-between the 4 black stones. ";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L4_Suicide_Exception()
    {
        String title = "4. Suicidal Exceptions";
        String content = " You may make suicidal moves if the resulting stone placement captures at least one enemy stone. "
                + "\n\n The capture makes the move non-suicidal since it prevents your stone from being immediately captured. "
                + "\n\n Try capturing a black stone.";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L5_Group_Capture1()
    {
        String title = "5. Normal Group Capture";
        String content = " You can capture multiple stones in one move by surrounding a group. "
                + "\n\n A group consists of any collection of stones on the board that are touching other stones "
                + "\n of the same color either vertically or horizontally, diagonal doesn’t count. "
                + "\n\n Try to capture this group of back stones.";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L6_Group_Capture2()
    {
        String title = "6. Alternate Group Capture";
        String content = " A capture is performed by removing a group’s liberties. "
                + "\n\n A liberty is any way in which a player can place a stone either within or adjacent to a group. "
                + "\n\n Try capturing this group of black stones.";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L7_Wall_Capture()
    {
        String title = "7. Wall Capture";
        String content = " In the case which a group is placed on the edge of the board "
                + "\n\n you can capture said group by surrounding it on every other side, essentially surrounding it against a wall. "
                + "\n\n This applies to groups sitting on corners as well. "
                + "\n\n All rules that apply to groups also apply to single stones. "
                + "\n\n Try capturing this wall bound group.";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L8_Rule_Of_Ko()
    {
        String title = "8. The Rule of Ko";
        String content = " In effort to prevent infinite loops of capturing the same two stones over and over again there is a special rule called the Rule of Ko. "
                + "\n\n Given the following board setup, assume Black just placed a stone at F5, capturing one of your white stones at E5. "
                + "\n\n You must wait one turn before you can place a stone at E5 again. "
                + "\n\n You may place a stone anywhere else on the board, you aren’t expected to skip your turn, but you can’t place a stone on E5 until your next turn. "
                + "\n\n Try it, see what happens…";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L9_Passing()
    {
        String title = "9. Passing";
        String content = " If for whatever reason a player does not wish to place an stones on the board during their turn they may simply skip that turn. "
                + "\n\n This is called Passing. "
                + "\n\n The game ends when both players pass consecutivley, one followed immediatley by the other. "
                + "\n\n Try passing your turn.";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L10_Scoring()
    {
        String title = "10. Territories and Scoring";
        String content = " This version of Go uses Area Scoring: Player Score = total stones on the board + total territories. \n\n A territory is any empty space on the board that is completley surrounded by stones of the same color. \n This includes spaces that sit on the edge of the board. \n\n Given the following board setup, assume that both players have just passed their turns and the game has ended. \n Black has 22 stones on the board and holds 15 territories resulting in a total score of 37. \n White has 20 stones on the board and holds 23 territories, \n plus white’s handicap of 0.5, resulting in a total score of 43.5. \n Therefore, white wins the game. \n\n (Note that position F6 cannot be claimed as a territory for either player since it is surrounded by different colored stones)";
        
        Lesson_Text_Box(title, content);
    }
    
    public void L11_White_Handicap()
    {
        String title = "11. More on White’s Handicap";
        String content = " White players always receive a handicap score of at least 0.5. "
                + "\n\n The reason for the .5 is to prevent ties. "
                + "\n\n It is not uncommon for players to select a different handicap for white greater than 0.5, "
                + "\n it is in fact encouraged as a way for less experianced players to be able to play with pros and still stand a chance. "
                + "\n\n The only caveat is that the handicap must always include the .5. "
                + "\n\n For our purposes our version of Go only allows for white handicaps that are less than 10 (0.5 – 9.5).";
        
        Lesson_Text_Box(title, content);
    }
    
    //Generates the secondary window
    public void Lesson_Text_Box(String title, String content)
    {
        Stage textBox = new Stage();
        textBox.setTitle(title);
        textBox.setMinWidth(250);
        
        Label lessonText = new Label();
        lessonText.setText(content);
        
        VBox layout = new VBox(10);
        layout.getChildren().addAll(lessonText);
        layout.setAlignment(Pos.TOP_LEFT);
        
        Scene scene = new Scene(layout);
        textBox.setScene(scene);
        textBox.showAndWait();
        
    }
}
