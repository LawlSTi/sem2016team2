/**
 *
 * @author Corey
 */
package go;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;

public class GoBoard {

    //Used to make the stone image appear at the correct location
    final double imageDrawOffsetX = 32;
    final double imageDrawOffsetY = 30;

    //Defines the user can click to place a stone at a particular intersection
    final double clickBoundX = 10;
    final double clickBoundY = 10;

    //xDif and yDif are how far apart each line is on the
    // x and y-axis on the Go board image
    final double xDif = 59;
    final double yDif = 65;

    //xOffset and yOffset are the coordinates for the top left-moste intersection.
    //These values are used in calculating where the user clicked.
    final double xOffset = 349;
    final double yOffset = 99;

    //The pane that will be used to draw the stone image files on
    AnchorPane board;

    public GoBoard(AnchorPane p) {
        board = p;
    }

    private void drawStone(double xCor, double yCor) {
        //Set the image file for the Go stone. Currently set to white stone for testing
        Image goPc = new Image("/Images/white_piece_transparent.png");
        ImageView v1 = new ImageView();

        //Set the size of the Go piece image
        v1.setFitWidth(65);
        v1.setFitHeight(65);
        v1.setImage(goPc);

        //Set the board image location on the scene
        board.setLeftAnchor(v1, xCor);
        board.setTopAnchor(v1, yCor);

        //Add the Go stone to the AnchorPane
        board.getChildren().add(v1);
    }

    //Will be used to set a white colored stone for tutorials
    public void setStoneWhite(double xCor, double yCor) {
        //TODO: Need to add specific image files
        coordDraw(xCor, yCor);
    }

    //Will be used to set a black colored stone for tutorials
    public void setStoneBlack(double xCor, double yCor) {
        //TODO: Need to add specific image files
        coordDraw(xCor, yCor);
    }

    //drawStoneIfInBounds is the method that does the actual drawing and checking if the 
    // coordinates that were clicked at are at a valid intersection.
    private void drawStoneIfInBounds(double xCor, double yCor, int rowNum, int colNum) {

        //Setting the row and column positions in the case that the clicked-on location is 
        // a valid location to place a stone.
        double rowPos = (rowNum * xDif) + xOffset;
        double colPos = (colNum * yDif) + yOffset;

        //Logic to tell if the location that is clicked on is one of the Go board's intersections
        if (rowNum >= 0 && colNum >= 0) {
            if (xCor >= (rowPos - clickBoundX) && xCor <= (rowPos + clickBoundX) && yCor >= (colPos - clickBoundY) && yCor <= (colPos + clickBoundY)) {
                drawStone(rowPos - imageDrawOffsetX, colPos - imageDrawOffsetY);
            }
        }
    }

    //coorDraw is the method called in the mouse event handler to draw stones
    private void coordDraw(double xCor, double yCor) {

        //rowNum and colNum are the x and y coordinates of where the 
        // user clicked on the board.
        int rowNum = findRowNum(xCor);
        int colNum = findColNum(yCor);

        drawStoneIfInBounds(xCor, yCor, rowNum, colNum);
    }

    private int findRowNum(double xCor) {
        //Using math to determine where the user clicked on the board
        return (int) Math.round((xCor - xOffset) / xDif);
    }

    private int findColNum(double yCor) {
        return (int) Math.round((yCor - yOffset) / yDif);
    }
}
