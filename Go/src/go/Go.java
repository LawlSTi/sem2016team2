/**
 * Project: Go Game for Software Engineering Management
 * File: Go.java
 * Creator: Chris Robinson
 * Date: 9/15/2016
 * Comments: This file contains our main() method. This file also establishes
             the first scene (Main Menu) upon launching the game, where the player may 
             make a selection based on preference.
 */

package go;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Tooltip;

public class Go extends Application {
    Stage menuStage;
    Scene menuScene;
     
    public static void main(String[] args) {
        launch(args);
    }

    Stage window;
 
    @Override
    public void start(Stage primaryStage) throws Exception {

        //Application title and constraints
        Stage window1 = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/GoFXML/mainmenu.fxml"));
        Scene scene = new Scene(root);
        window1.setScene(scene);
        window1.setResizable(false);
        window1.sizeToScene();
        window1.show();
        window1.setTitle("Go!");

    }
}