/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package go;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Chris
 */
public class MainmenuController implements Initializable {
 
    @FXML
    private Button Quit;
    @FXML
    private Button About;
    @FXML
    private Button tutorials;
    @FXML
    private Button playgame;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void LaunchAbout(ActionEvent event) {
        AboutPage aboutscene = new AboutPage();
    }

    @FXML
    private void LaunchTutorials(ActionEvent event) {
        try {
            //mainStage references the tutorial page 
            Stage mainStage = (Stage)tutorials.getScene().getWindow();
            //Grabbing the FXML file for the tutorial select page
            Parent root = FXMLLoader.load(Go.class.getResource("/GoFXML/tutorial_select.fxml"));
            
            //Setting the new stage and grabbing the .css file for the tutorial select page
            Scene tutorialSelectScene = new Scene(root);
            tutorialSelectScene.getStylesheets().add("/GoCSS/tutorialSelectStylesheet.css");
            mainStage.setScene(tutorialSelectScene);
            mainStage.show();
        }
        catch (Exception e) {/*Do nothing, like a boss*/
            e.printStackTrace();//used in testing
        }
    }

    @FXML
    private void QuitGame(ActionEvent event) {
        System.exit(0);
    }
    @FXML
    private void LaunchGame(ActionEvent event) throws IOException {

        //mainStage is referencing the main menu page
        Stage mainStage = (Stage)playgame.getScene().getWindow();
        //Grabbing the FXML file for the Go board page
        Parent root = FXMLLoader.load(Go.class.getResource("/GoFXML/GoBoard.fxml")); 

        Scene gameScene = new Scene(root);
        mainStage.setScene(gameScene);
        mainStage.show();    
    }

}
